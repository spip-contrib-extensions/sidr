<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-sidr
// Langue: fr
// Date: 29-03-2020 21:04:09
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'sidr_description' => 'Menu de côté responsive basé sur le plugin jQuery [Sidr->http://www.berriart.com/sidr/].',
	'sidr_slogan' => 'Menu de côté responsive',
);
?>